# Readme #

![ColorConverterX.png](https://bitbucket.org/repo/RAkMn6/images/1780834828-ColorConverterX.png)

A minimal app that converts RGB color values between Hex, 0-1 and 0-255. Just change any of the textfields and all the others will update accordingly. The last field is source-code, ready for pasting into Unity.

### How do I get set up? ###

* Open ColorConverterX.sln in Visual Studio Express 2013 for Windows Desktop.
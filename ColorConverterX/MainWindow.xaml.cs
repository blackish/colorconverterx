﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using System.Drawing;

namespace ColorConverterX
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private Color color;
        private SolidColorBrush colorBrush = new SolidColorBrush();


        public MainWindow()
        {
            InitializeComponent();
            Console.WriteLine("Startup: " + hexField.Text);
            UpdateColorsFromHex("#CCC");
        }


        private void ApplyColor() {
            colorBrush.Color = color;
            colorBox.Fill = colorBrush;
            RefillAllValues();
        }


        private void RefillAllValues()
        {
            Console.WriteLine("RefillAllValues");
            if (sourceField != null) if(!sourceField.IsFocused) sourceField.Text = "new Color(" + color.ScR.ToString("0.##") + "f, " + color.ScG.ToString("0.##") + "f, " + color.ScB.ToString("0.##") + "f " + color.ScA.ToString("0.##") + "f)";
            if (r01Field != null) if (!r01Field.IsFocused) r01Field.Text = color.ScR.ToString("0.###");
            if (g01Field != null) if (!g01Field.IsFocused) g01Field.Text = color.ScG.ToString("0.###");
            if (b01Field != null) if (!b01Field.IsFocused) b01Field.Text = color.ScB.ToString("0.###");
            if (alphaField != null) if (!alphaField.IsFocused) alphaField.Text = color.ScA.ToString("0.###");
            if (r0255Field != null) if (!r0255Field.IsFocused) r0255Field.Text = color.R.ToString("0");
            if (g0255Field != null) if (!g0255Field.IsFocused) g0255Field.Text = color.G.ToString("0");
            if (b0255Field != null) if (!b0255Field.IsFocused) b0255Field.Text = color.B.ToString("0");
            if (a0255Field != null) if (!a0255Field.IsFocused) a0255Field.Text = color.A.ToString("0");
            if (hexField != null) if (!hexField.IsFocused) hexField.Text = "#" + color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
        }



        //Change Color
        #region ValueChanges

        private void UpdateColorsFromHex(string str)
        {

            try
            {
                float alpha = color.ScA;
                color = (Color)ColorConverter.ConvertFromString(str);
                color.ScA = alpha;
            }
            catch
            {
                Console.WriteLine("Could not convert hex color");
                return;
            }

            ApplyColor();
        }

        private void R01Changed() 
        {
            if (r01Field != null)
            { 
                float val = 0f;
                if (float.TryParse(r01Field.Text, out val)) 
                {
                    color.ScR = val;
                    ApplyColor();
                }
            }
        }

        private void R0255Changed() 
        {
            if (r0255Field != null)
            {
                byte val = 0;
                if (byte.TryParse(r0255Field.Text, out val))
                {
                    color.R = val;
                    ApplyColor();
                }
            }
        }

        private void G01Changed()
        {
            if (g01Field != null)
            {
                float val = 0f;
                if (float.TryParse(g01Field.Text, out val))
                {
                    color.ScG = val;
                    ApplyColor();
                }
            }
        }

        private void G0255Changed()
        {
            if (g0255Field != null)
            {
                byte val = 0;
                if (byte.TryParse(g0255Field.Text, out val))
                {
                    color.G = val;
                    ApplyColor();
                }
            }
        }

        private void B01Changed()
        {
            if (b01Field != null)
            {
                float val = 0f;
                if (float.TryParse(b01Field.Text, out val))
                {
                    color.ScB = val;
                    ApplyColor();
                }
            }
        }

        private void B0255Changed()
        {
            if (b0255Field != null)
            {
                byte val = 0;
                if (byte.TryParse(b0255Field.Text, out val))
                {
                    color.B = val;
                    ApplyColor();
                }
            }
        }

        private void UpdateAlpha()
        {
            if (alphaField != null)
            {
                float alpha = 1f;
                if (float.TryParse(alphaField.Text, out alpha))
                {
                    color.ScA = alpha;
                    ApplyColor();
                }
            }
        }

        private void UpdateAlpha255()
        {
            if (a0255Field != null)
            {
                byte alpha = 255;
                if (byte.TryParse(a0255Field.Text, out alpha))
                {
                    color.A = alpha;
                    ApplyColor();
                }
            }
        }

        #endregion


        //DETECT CHANGES
        #region ChangeDetection

        private void hexField_LostFocus(object sender, RoutedEventArgs e)
        {
            Console.WriteLine("Here");
            UpdateColorsFromHex(hexField.Text);
        }

        private void hexField_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateColorsFromHex(hexField.Text);
        }


        private void r01Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            R01Changed();
        }
        
        private void r01Field_LostFocus(object sender, RoutedEventArgs e)
        {
            R01Changed();
        }


        private void g01Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            G01Changed();
        }

        private void g01Field_LostFocus(object sender, RoutedEventArgs e)
        {
            G01Changed();
        }


        private void b01Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            B01Changed();
        }

        private void b01Field_LostFocus(object sender, RoutedEventArgs e)
        {
            B01Changed();
        }


        private void r0255Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            R0255Changed();
        }

        private void r0255Field_LostFocus(object sender, RoutedEventArgs e)
        {
            R0255Changed();
        }

        private void g0255Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            G0255Changed();
        }

        private void g0255Field_LostFocus(object sender, RoutedEventArgs e)
        {
            G0255Changed();
        }

        private void b0255Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            B0255Changed();
        }

        private void b0255Field_LostFocus(object sender, RoutedEventArgs e)
        {
            B0255Changed();
        }


        private void alphaField_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateAlpha();
        }

        private void alphaField_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateAlpha();
        }


        private void a0255Field_LostFocus(object sender, RoutedEventArgs e)
        {
            UpdateAlpha255();
        }

        private void a0255Field_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateAlpha255();
        }


        #endregion



    }
}
